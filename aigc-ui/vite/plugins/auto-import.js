// yangbuyi Copyright (c) https://yby6.com 2023.

import autoImport from 'unplugin-auto-import/vite';

export default function createAutoImport() {
  return autoImport({
    include: [
      /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
      /\.vue$/,
      /\.vue\?vue/, // .vue
      /\.md$/ // .md
    ],
    imports: ['vue', 'vue-router'], // 自动导入
    // 这个一定要配置，会多出一个auto-import.d.ts文件，
    dts: 'vite/plugins/auto-import.d.ts'
  });
}
