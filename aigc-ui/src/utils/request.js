// yangbuyi Copyright (c) https://yby6.com 2023.

import axios from 'axios';
import { ElMessage } from 'element-plus';

// 创建axios实例
const service = axios.create({
  baseURL: '/aigc-api', // 部署 api 的 base_url
  timeout: 20000 // 请求超时时间
});

// request拦截器
service.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    // Do something with request error
    Promise.reject(error);
  }
);

// response 拦截器
service.interceptors.response.use(
  (response) => {
    const res = response.data;
    // 视频流
    if (response.data.type === 'application/octet-stream') {
      return response.data;
    } else if (res.code === 201 || res.code === 200) {
      // 201 表示请求成功自定义处理问题
      return response.data;
    } else {
      ElMessage({
        message: res.message || res.msg,
        type: 'error',
        duration: 5 * 1000
      });
      return Promise.reject('error');
    }
  },
  (error) => {
    ElMessage({
      message: error.code === 'ERR_BAD_RESPONSE' ? '服务器无法响应!' : error.message || error.msg,
      type: 'error',
      duration: 5 * 1000
    });
    return Promise.reject(error);
  }
);

export default service;
