// yangbuyi Copyright (c) https://yby6.com 2023.

// main.ts
import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import locale from 'element-plus/es/locale/lang/zh-cn'; // 导入中文语言包
import { createI18n } from 'vue-i18n';
import zhCnMessages from './locales/zh-cn'; // 导入自己创建的语言文件
import enMessages from './locales/en'; // 导入英文语言文件
import 'element-plus/dist/index.css';
import './assets/css/index.scss'; // global css
import './styles/index.scss';
import App from './App.vue';
// 引入路由器
import router from './router';
// 注册指令
import plugins from './plugins';

const i18n = createI18n({
  locale: 'zh-cn',
  messages: {
    'zh-cn': zhCnMessages,
    en: enMessages
  }
});

const app = createApp(App);

app.use(ElementPlus, {
  locale
});
app.use(i18n);
app.use(router);
app.use(plugins);

app.mount('#app');
