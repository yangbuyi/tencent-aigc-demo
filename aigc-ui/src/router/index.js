// yangbuyi Copyright (c) https://yby6.com 2023.

import {createRouter, createWebHistory} from 'vue-router';
import Index from "@/views/index.vue";
import ImgToImg from "@/views/imgToimg.vue";
// 引入组件


const constantRoutes = [
  {
    path: '/',
    component: Index
  },
  {
    path: '/imgToimg',
    component: ImgToImg
  },


];

const router = createRouter({
  history: createWebHistory(),
  routes: constantRoutes
});

export default router;
