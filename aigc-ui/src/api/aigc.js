// yangbuyi Copyright (c) https://yby6.com 2023.

import request from '@/utils/request';

/**
 * 文字转图片
 * @param data
 * @returns {*}
 */
export function textToImage(data) {
    return request({
        url: '/aigc/textToImage',
        method: 'post',
        data
    });
}

export function test() {
    return request({
        url: '/aigc/test',
        method: 'get'
    });
}

