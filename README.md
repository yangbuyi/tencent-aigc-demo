# 文章首发腾讯云开发者社区
[《这次不从零了五万字带您,从负零玩转腾讯AI绘画图像生成搭建前后端分离项目》](https://cloud.tencent.com/developer/article/2369120)

# 一、前言

最近 AIGC 火出圈了还记得上次在群里面的大佬们聊到了 AIGC 的话题我滴妈涉及到我的知识盲区了知识听过这个东西就是生成图片所以不怎么感冒.然后最近腾讯云开发者社区发布了腾讯云AI技术专家与传智教育人工智能学科高级技术专家正在联合打造《腾讯云AI绘画-StableDiffusion图像生成》训练营，训练营将通过8小时的学习带你玩转AI绘画的教程我也就去深入了一下. 感兴趣的大佬快来报名学习呀~

点击报名，抢先学习[《腾讯云AI绘画-StableDiffusion图像生成》训练营 八小时玩转AI绘画](https://cloud.tencent.com/developer/learning/camp/19?from_column=20421&from=20421)

> 💗 本篇文章以`五万三千零三十二字`带您玩转腾讯AI绘画图像生成搭建前后端分离项目
>  `如果觉得本篇文章帮助到了您喜欢本篇文章那就点赞支持一下吧!!!!!` 💗 
> 本篇教程我也是一边写文章一边写代码,一起来吧!!!!! 


## 技术架构图

> 技术选型:
> 使用了前端 Vue3 +Vite来搭建前端架构.
> 后端使用Java语言的SpringBoot3框架来搭建服务.
> 最后我会使用语音系统在用户生成完毕图片后生成语音提醒生成成功
> (因为如果图片质量较高生成是要有一定的耗时的!!!!)

![输入图片说明](img/image2.png)

**前端效果演示**

> 注意⚠️: 前端样式仿造腾讯云智能图像创作平台的UI
> 地址: [https://ti-image.cloud.tencent.com/text-to-image/](https://ti-image.cloud.tencent.com/text-to-image/)

![输入图片说明](img/image3.png)

> 点击报名，抢先学习[《腾讯云AI绘画-StableDiffusion图像生成》训练营](https://cloud.tencent.com/developer/learning/camp/19?from_column=20421&from=20421)

![输入图片说明](img/image4.png)
