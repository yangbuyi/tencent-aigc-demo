package com.yby6.tencentaigc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TencentAigcApplication {

    public static void main(String[] args) {
        SpringApplication.run(TencentAigcApplication.class, args);
    }

}
