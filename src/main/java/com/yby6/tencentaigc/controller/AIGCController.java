package com.yby6.tencentaigc.controller;

import com.tencentcloudapi.aiart.v20221229.models.TextToImageResponse;
import com.yby6.tencentaigc.domain.ImageToImage;
import com.yby6.tencentaigc.domain.TextToImage;
import com.yby6.tencentaigc.resp.R;
import com.yby6.tencentaigc.utils.AIGCUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 人工智能内容生成接口
 *
 * @author Yang Shuai
 * Create By 2023/12/10
 */
@RestController
@RequestMapping("/aigc")
public class AIGCController {

    /**
     * 文本转图像请求
     *
     * @return {@link R}<{@link TextToImageResponse}>
     */
    @PostMapping("textToImage")
    public R<TextToImageResponse> textToImage(@RequestBody TextToImage request) {
        return R.ok(AIGCUtils.textToImage(request));
    }


    @PostMapping("imageToImage")
    public R<String> imageToImage(@RequestBody ImageToImage imageToImage) {

        AIGCUtils.imageToImage(imageToImage);

        return R.ok("imageToImage");
    }

    @GetMapping("test")
    public R<String> test() {
        return R.ok("test");
    }

}
