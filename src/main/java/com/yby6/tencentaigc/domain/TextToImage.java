package com.yby6.tencentaigc.domain;

import lombok.Data;

/**
 * 文本转图像参数类
 *
 * @author Yang Shuai
 * Create By 2023/12/10
 */
@Data
public class TextToImage {

    /**
     * 文本描述。
     * 算法将根据输入的文本智能生成与之相关的图像。建议详细描述画面主体、细节、场景等，文本描述越丰富，生成效果越精美。
     * 不能为空，推荐使用中文。最多可传256个 utf-8 字符。
     */
    private String prompt;

    /**
     * 反向文本描述。
     * 用于一定程度上从反面引导模型生成的走向，减少生成结果中出现描述内容的可能，但不能完全杜绝。
     * 推荐使用中文。最多可传256个 utf-8 字符。
     */
    private String negativePrompt;

    /**
     * 绘画风格。
     * 请在 [智能文生图风格列表](https://cloud.tencent.com/document/product/1668/86249) 中选择期望的风格，传入风格编号。
     * 推荐使用且只使用一种风格。不传默认使用201（日系动漫风格）。
     */
    private String styles ;

    /**
     * 生成图结果的配置，包括输出图片分辨率和尺寸等。
     * <p>
     * 支持生成以下分辨率的图片：768:768（1:1）、768:1024（3:4）、1024:768（4:3）、1024:1024（1:1）、720:1280（9:16）、1280:720（16:9）、768:1280（3:5）、1280:768（5:3）、1080:1920（9:16）、1920:1080（16:9），不传默认使用768:768。
     */
    private String resolution;


    /**
     * 返回图像方式（base64 或 url) ，二选一，默认为 base64。url 有效期为1小时。
     */
    // private String rspImgType = "url";


}