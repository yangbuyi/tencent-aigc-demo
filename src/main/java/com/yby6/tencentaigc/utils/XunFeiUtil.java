// yangbuyi Copyright (c) https://yby6.com 2023.

package com.yby6.tencentaigc.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.Getter;
import okhttp3.*;
import okio.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 讯飞WebApi语音合成
 *
 * @author Yang Shuai
 * Create By 2023/12/18
 */
@Component
public class XunFeiUtil {
    protected static final Logger log = LoggerFactory.getLogger(XunFeiUtil.class);
    private static String hostUrl;

    @Value("${xunfei.hostUrl}")
    public void setHostUrl(String hostUrl) {
        XunFeiUtil.hostUrl = hostUrl;
    }

    private static String appid;

    @Value("${xunfei.appid}")
    public void setAppid(String appid) {
        XunFeiUtil.appid = appid;
    }

    private static String apiSecret;

    @Value("${xunfei.apisecret}")
    public void setApiSecret(String apiSecret) {
        XunFeiUtil.apiSecret = apiSecret;
    }

    private static String apiKey;

    @Value("${xunfei.apikey}")
    public void setApiKey(String apiKey) {
        XunFeiUtil.apiKey = apiKey;
    }

    public static final Gson json = new Gson();
    private static String base64 = "";
    private static volatile boolean lock = true;

    /**
     * 将文本转换为MP3格语音base64文件
     *
     * @param text 要转换的文本（如JSON串）
     * @return 转换后的base64文件
     */
    public static String convertText(String text) throws Exception {
        lock = true;
        base64 = "";
        // 构建鉴权url
        String authUrl = getAuthUrl(hostUrl, apiKey, apiSecret);
        OkHttpClient client = new OkHttpClient.Builder().build();
        //将url中的 schema http://和https://分别替换为ws:// 和 wss://
        String url = authUrl.replace("http://", "ws://").replace("https://", "wss://");
        Request request = new Request.Builder().url(url).build();
        List<byte[]> list = new LinkedList<>();
        WebSocket webSocket = client.newWebSocket(request, new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, Response response) {
                super.onOpen(webSocket, response);
                log.info("链接开始合成音频:{}", response.body());
                //发送数据
                JsonObject frame = new JsonObject();
                JsonObject business = new JsonObject();
                JsonObject common = new JsonObject();
                JsonObject data = new JsonObject();
                // 填充common
                common.addProperty("app_id", appid);
                //填充business,AUE属性lame是MP3格式，raw是PCM格式
                business.addProperty("aue", "lame");
                business.addProperty("sfl", 1);
                business.addProperty("tte", "UTF8");//小语种必须使用UNICODE编码
                business.addProperty("vcn", "xiaoyan");//到控制台-我的应用-语音合成-添加试用或购买发音人，添加后即显示该发音人参数值，若试用未添加的发音人会报错11200
                business.addProperty("pitch", 50);
                business.addProperty("speed", 50);
                //填充data
                data.addProperty("status", 2);//固定位2
                data.addProperty("text", Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8)));
                //使用小语种须使用下面的代码，此处的unicode指的是 utf16小端的编码方式，即"UTF-16LE"”
                //data.addProperty("text", Base64.getEncoder().encodeToString(text.getBytes("UTF-16LE")));
                //填充frame
                frame.add("common", common);
                frame.add("business", business);
                frame.add("data", data);
                webSocket.send(frame.toString());
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                super.onMessage(webSocket, text);
                //处理返回数据
                log.info("开始处理文本合成音频");
                ResponseData resp = null;
                try {
                    resp = json.fromJson(text, ResponseData.class);
                } catch (Exception e) {
                    log.error("异常:", e);
                }
                if (resp != null) {
                    if (resp.getCode() != 0) {
                        log.error("error=>" + resp.getMessage() + " sid=" + resp.getSid());
                        return;
                    }
                    if (resp.getData() != null) {
                        String result = resp.getData().audio;
                        byte[] audio = Base64.getDecoder().decode(result);
                        list.add(audio);
                        // 说明数据全部返回完毕，可以关闭连接，释放资源
                        if (resp.getData().status == 2) {
                            String is = base64Concat(list);
                            base64 = is;
                            lock = false;
                            webSocket.close(1000, "");
                        }
                    }
                }
            }

            @Override
            public void onMessage(WebSocket webSocket, ByteString bytes) {
                super.onMessage(webSocket, bytes);
            }

            @Override
            public void onClosing(WebSocket webSocket, int code, String reason) {
                super.onClosing(webSocket, code, reason);
            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {
                super.onClosed(webSocket, code, reason);
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                super.onFailure(webSocket, t, response);
            }
        });
        while (lock) {
        }
        return base64;
    }

    /**
     * * base64拼接
     */
    static String base64Concat(List<byte[]> list) {
        int length = 0;
        for (byte[] b : list) {
            length += b.length;
        }
        int len = 0;
        byte[] retByte = new byte[length];
        for (byte[] b : list) {
            concat(len, retByte, b);
            len += b.length;
        }
        return cn.hutool.core.codec.Base64.encode(retByte);
    }

    static byte[] concat(int len, byte[] a, byte[] b) {
        for (byte value : b) {
            a[len] = value;
            len++;
        }
        return a;
    }

    /**
     * * 获取权限地址
     * *
     * * @param hostUrl
     * * @param apiKey
     * * @param apiSecret
     * * @return
     */
    private static String getAuthUrl(String hostUrl, String apiKey, String apiSecret) throws Exception {
        URL url = new URL(hostUrl);
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = format.format(new Date());
        StringBuilder builder = new StringBuilder("host: ").append(url.getHost()).append("\n").
                append("date: ").append(date).append("\n").
                append("GET ").append(url.getPath()).append(" HTTP/1.1");
        Charset charset = StandardCharsets.UTF_8;
        Mac mac = Mac.getInstance("hmacsha256");
        SecretKeySpec spec = new SecretKeySpec(apiSecret.getBytes(charset), "hmacsha256");
        mac.init(spec);
        byte[] hexDigits = mac.doFinal(builder.toString().getBytes(charset));
        String sha = Base64.getEncoder().encodeToString(hexDigits);
        String authorization = String.format("hmac username=\"%s\", algorithm=\"%s\", headers=\"%s\", signature=\"%s\"", apiKey, "hmac-sha256", "host date request-line", sha);
        HttpUrl httpUrl = HttpUrl.parse("https://" + url.getHost() + url.getPath()).newBuilder().
                addQueryParameter("authorization", Base64.getEncoder().encodeToString(authorization.getBytes(charset))).
                addQueryParameter("date", date).
                addQueryParameter("host", url.getHost()).
                build();
        return httpUrl.toString();
    }


    @Getter
    public static class ResponseData {
        private int code;
        private String message;
        private String sid;
        private Data data;

    }

    private static class Data {
        //标志音频是否返回结束  status=1，表示后续还有音频返回，status=2表示所有的音频已经返回
        private int status;
        //返回的音频，base64 编码
        private String audio;
        // 合成进度
        private String ced;
    }
}
