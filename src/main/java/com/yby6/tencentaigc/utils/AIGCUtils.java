package com.yby6.tencentaigc.utils;

import com.google.gson.Gson;
import com.tencentcloudapi.aiart.v20221229.AiartClient;
import com.tencentcloudapi.aiart.v20221229.models.*;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.yby6.tencentaigc.config.TencentConfig;
import com.yby6.tencentaigc.domain.ImageToImage;
import com.yby6.tencentaigc.domain.TextToImage;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * AI GC 工具 人工智能内容生成
 *
 * @author Yang Shuai
 * Create By 2023/12/10
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class AIGCUtils {
    private static AIGCUtils aigcUtils;
    private final TencentConfig tencentConfig;

    /**
     * 要在静态方法里面调用IOC容器的bean对象
     * PostConstruct 在构造函数执行之后执行。
     * 可以方便的把注入的bean对象给到静态属性
     * 源码： AutowiredAnnotationBeanPostProcessor 的 buildAutowiringMetadata 函数
     * isStatic 绕过了静态不进行注入
     */
    @PostConstruct
    public void postConstruct() {
        aigcUtils = this;
    }


    /**
     * 获取 腾讯云 AIGC客户请求
     */
    public static AiartClient getAiartClient() {
        Credential cred = new Credential(aigcUtils.tencentConfig.getSecretId(), aigcUtils.tencentConfig.getSecretKey());
        // 实例化一个http选项，可选的，没有特殊需求可以跳过
        HttpProfile httpProfile = new HttpProfile();
        // api地址
        httpProfile.setEndpoint("aiart.tencentcloudapi.com");
        // 实例化一个client选项，可选的，没有特殊需求可以跳过
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        // 实例化要请求产品的client对象,clientProfile是可选的
        return new AiartClient(cred, "ap-shanghai", clientProfile);

    }

    /**
     * 腾讯云 AIGC 文本生成图片
     * <a href="https://console.cloud.tencent.com/api/explorer?Product=aiart&Version=2022-12-29&Action=TextToImage">...</a>
     *
     * @param req {@link TextToImage}
     * @return {@link TextToImageResponse}
     */
    public static TextToImageResponse textToImage(TextToImage req) {
        TextToImageRequest request = new TextToImageRequest();
        // 文本描述
        request.setPrompt(req.getPrompt());
        // 反向文本描述
        request.setNegativePrompt(req.getNegativePrompt());
        // 图片风格
        request.setStyles(new String[]{req.getStyles()});
        // 分辨率
        ResultConfig config = new ResultConfig();
        config.setResolution(req.getResolution());
        request.setResultConfig(config);
        // 返回图像方式（base64 或 url) ，二选一，默认为 base64。url 有效期为1小时。
        request.setRspImgType("url");
        try {
            log.info("请求参数: {}", new Gson().toJson(request));
            TextToImageResponse image = getAiartClient().TextToImage(request);
            log.info("返回参数: {}", image.getResultImage());
            return image;
        } catch (TencentCloudSDKException e) {
            log.error("腾讯云 AIGC 文本生成图片异常", e);
            throw new RuntimeException("腾讯云 AIGC 文本生成图片异常");
        }
    }

    /**
     * 腾讯云 AIGC 图生图
     * <a href="https://console.cloud.tencent.com/api/explorer?Product=aiart&Version=2022-12-29&Action=ImageToImage">...</a>
     *
     * @param imageToImage {@link ImageToImage}
     * @return {@link ImageToImageResponse}
     */
    public static ImageToImageResponse imageToImage(ImageToImage imageToImage) {
        final AiartClient aiartClient = getAiartClient();
        // 实例化一个请求对象,每个接口都会对应一个request对象
        ImageToImageRequest req = new ImageToImageRequest();
        //  Base64 和 Url 必须提供一个，如果都提供以 Base64 为准
        req.setInputImage(imageToImage.getInputImage());
        // 文本描述。 用于在输入图的基础上引导生成图效果，增加生成结果中出现描述内容的可能。 推荐使用中文。最多支持256个 utf-8 字符。
        req.setPrompt(imageToImage.getPrompt());
        // 反向描述
        req.setNegativePrompt(imageToImage.getNegativePrompt());
        // 风格
        req.setStyles(new String[]{imageToImage.getStyles()});
        // 分辨率
        final ResultConfig config = new ResultConfig();
        config.setResolution(imageToImage.getResolution());
        req.setResultConfig(config);
        // 返回格式
        req.setRspImgType("url");

        // 比例值
        String logoRect = imageToImage.getResolution().replaceAll("（.*?）", "");
        String[] logoRectArr = logoRect.split(":");

        // 图片的水印
        LogoParam logoParam1 = new LogoParam();
        logoParam1.setLogoUrl("https://cdn.nlark.com/yuque/0/2023/png/2426233/1702196852392-3083990a-0590-4299-b89c-6c915f2b8a7a.png");
        LogoRect logoRect1 = new LogoRect();
        logoRect1.setWidth(71L); // 水印的宽度
        logoRect1.setHeight(20L); // 水印的高度
        // 水印的位置
        logoRect1.setX(Integer.parseInt(logoRectArr[0]) - logoRect1.getWidth()); // 比例宽 - 水印的宽度
        logoRect1.setY(Integer.parseInt(logoRectArr[1]) - logoRect1.getHeight()); // 比例高 - 水印的高度
        logoParam1.setLogoRect(logoRect1);
        req.setLogoParam(logoParam1);
        log.info("请求参数: {}", new Gson().toJson(req));
//        try {
//            log.info("请求参数: {}", new Gson().toJson(req));
//            // 返回的resp是一个ImageToImageResponse的实例，与请求对象对应
//            ImageToImageResponse resp = aiartClient.ImageToImage(req);
//            log.info("返回参数: {}", resp.getResultImage());
//            return resp;
//        } catch (TencentCloudSDKException e) {
//            log.error("腾讯云 AIGC 图生成图片异常", e);
//            throw new RuntimeException("腾讯云 AIGC 图生成图片异常");
//        }

        return null;
    }

}
