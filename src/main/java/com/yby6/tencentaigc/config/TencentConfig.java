package com.yby6.tencentaigc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * 腾讯云yml配置
 *
 * @author Yang Shuai
 * Create By 2023/12/10
 */
@Data
@Component
@ConfigurationProperties(prefix = "tencent")
public class TencentConfig {
    private String appId;
    private String secretId;
    private String secretKey;
}
