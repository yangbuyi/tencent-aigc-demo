package com.yby6.tencentaigc;

import com.tencentcloudapi.aiart.v20221229.AiartClient;
import com.tencentcloudapi.aiart.v20221229.models.*;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
class TencentAigcApplicationTests {

    @Value("${tencent.secretId}")
    private String secretId;

    @Value("${tencent.secretKey}")
    private String secretKey;

    @Test
    void contextLoads() throws TencentCloudSDKException {
//        final Credential cred = new Credential(secretId, secretKey);
//
//        // 实例化一个http选项，可选的，没有特殊需求可以跳过
//        HttpProfile httpProfile = new HttpProfile();
//        httpProfile.setEndpoint("aiart.tencentcloudapi.com");
//        // 实例化一个client选项，可选的，没有特殊需求可以跳过
//        ClientProfile clientProfile = new ClientProfile();
//        clientProfile.setHttpProfile(httpProfile);
//        // 实例化要请求产品的client对象,clientProfile是可选的
//        AiartClient client = new AiartClient(cred, "ap-shanghai", clientProfile);
//
//        TextToImageRequest req = new TextToImageRequest();
//
//        // 根据文本描述生成对应的图片
//        req.setPrompt("3D,帮我生成一个在海边跳舞的小女孩,还里面有可爱的鱼群,天空上飞舞的百灵鸟");
//
//        // 图片的比例大小
//        ResultConfig resultConfig1 = new ResultConfig();
//        resultConfig1.setResolution("1920:1080");
//        req.setResultConfig(resultConfig1);
//
//        // 图片的水印
//        LogoParam logoParam1 = new LogoParam();
//        logoParam1.setLogoUrl("https://cdn.nlark.com/yuque/0/2023/png/2426233/1702196852392-3083990a-0590-4299-b89c-6c915f2b8a7a.png");
//        LogoRect logoRect1 = new LogoRect();
//        logoRect1.setX(697L); // 比例宽 - 水印的宽度
//        logoRect1.setY(748L); // 比例高 - 水印的高度
//        logoRect1.setWidth(71L); // 水印的宽度
//        logoRect1.setHeight(20L); // 水印的高度
//        logoParam1.setLogoRect(logoRect1);
//        req.setLogoParam(logoParam1);
//
//        // 返回的是URL
//        req.setRspImgType("url");
//        // 返回的resp是一个TextToImageResponse的实例，与请求对象对应
//        final TextToImageResponse resp = client.TextToImage(req);
//
//
//        // 输出json格式的字符串回包
//        // final String resultImage = "data:image/jpeg;base64," + resp.getResultImage();
//        System.out.println(TextToImageResponse.toJsonString(resp));

    }


}
